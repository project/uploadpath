<?php

/**
 * Implementation of hook_menu()
 *
 */
function uploadpath_menu($may_cache) {
  
  $items = array();
  
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/uploadpaths',
      'title' => t('File upload paths'),
      'description' => t('Configure a prefix to apply to the path of uploaded files.'),
      'access' => user_access('administer site configuration'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('uploadpath_admin_settings'),
      'type' => MENU_NORMAL_ITEM,
    );
  }
  
  return $items;
}

/**
 * Configuration callback for this module
 *
 */
function uploadpath_admin_settings() {
  
  $form = array();
  
  $form['uploadpath_prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Pattern for the file prefix'),
      '#description' => t('Specify the pattern to prefix to file names uploaded with the upload module.  It will be appended after the site files directory (e.g., files) but before the file name itself.  Do not include a leading or trailing slash.  Spaces will be converted to underscores to avoid file system issues.'),
      '#default_value' => variable_get('uploadpath_prefix', ''),
    );
  
  $form['token_help'] = array(
      '#title' => t('Replacement patterns'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description' => t('Prefer raw-text replacements for text to avoid problems with HTML entities!'),
    );
    $form['token_help']['help'] = array(
      '#value' => theme('token_help', 'node'),
    );
  
  
  return system_settings_form($form);
}

/**
 * Implementation of hook_nodeapi()
 *
 */
function uploadpath_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'submit':
      if (isset($node->files)) {
        foreach ($node->files as $key => $file) {
          if (0 === strpos($key, 'upload_')) {  // Only rewrite the name when adding the file, not when updating it
            // Get the new, prefixed file name
            $file_name = str_replace(array(' ', "\n", "\t"), '_', token_replace(variable_get('uploadpath_prefix', '') . '/', 'node', $node)) . $node->files[$key]['filename'];
            
            // Create the directory if it doesn't exist yet.
            // This will also error out and therefore not modify the file name if there is a problem.
            if (file_check_directory(dirname(file_directory_path() . '/' . $file_name), FILE_CREATE_DIRECTORY)) {
              $node->files[$key]['filename'] = $file_name;
            }
          }
        }
      }
      break;
  }
}